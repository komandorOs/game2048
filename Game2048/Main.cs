﻿using System;
using System.Windows.Forms;

using Game2048.Model;
using Game2048.View;

namespace Game2048
{
	static class Program
	{
		[STAThread]
		static void Main()
		{
			var gameConfig = new GameConfiguration(4);
			var gameModel = new GameModel(gameConfig, Properties.Settings.Default.BestScore);
			var gameForm = new GameForm(gameModel);
			Application.Run(gameForm);
		}
	}
}
