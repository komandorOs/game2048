﻿using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Text;

using Game2048.Controls;
using Game2048.Model;
using Game2048.Properties;
using System.Collections.Generic;

namespace Game2048.View
{
	class GameForm : Form
	{
		//model
		private GameModel m_gameModel;

		//window parameters
		private PrivateFontCollection m_customFonts;

		//controls
		private TableLayoutPanel m_LayoutPanel;
		private TableLayoutPanel m_UIPanel;
		private TableLayoutPanel m_UIScorePanel;
		private TableLayoutPanel m_FieldPanel;
		private Label m_scoreValueLabel;
		private Label m_bestScoreValueLabel;

		public GameForm(GameModel gameModel)
		{
			m_gameModel = gameModel;

			//form setup
			InitForm();

			m_LayoutPanel = new TableLayoutPanel();
			m_LayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 20f));
			m_LayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 80f));
			m_LayoutPanel.Dock = DockStyle.Fill;
			m_LayoutPanel.BackColor = Colors.BackgroundColor;
			Controls.Add(m_LayoutPanel);

			//ui controls setup
			InitUI();

			//field controls setup
			InitField();

			//subscribe events
			m_gameModel.StateChanged += OnStateChange;
			m_gameModel.FieldChanged += OnFieldChange;

			//start game
			m_gameModel.Start();
		}

		private void InitForm()
		{
			ClientSize = new Size(800, 1000);
			FormBorderStyle = FormBorderStyle.FixedSingle;
			MaximizeBox = false;
			MinimizeBox = false;
			DoubleBuffered = true;
			StartPosition = FormStartPosition.CenterScreen;
			this.Icon = new Icon("../../Resources/icon.ico");
			this.Text = "2048";
			InitFonts();
		}

		private void InitFonts()
		{
			m_customFonts = new PrivateFontCollection();
			m_customFonts.AddFontFile("../../Resources/Proxima Nova Bold.otf");
		}

		private void InitUI()
		{
			m_UIPanel = new TableLayoutPanel();
			m_UIPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 40f));
			m_UIPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 0f));
			m_UIPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50f));
			m_UIPanel.BackColor = Colors.BackgroundColor;
			m_UIPanel.Dock = DockStyle.Fill;
			m_LayoutPanel.Controls.Add(m_UIPanel, 0, 0);

			//game name label
			var nameLabel = new Label();
			nameLabel.Font = new Font(m_customFonts.Families[0], 60, FontStyle.Bold);
			nameLabel.AutoSize = false;
			nameLabel.BackColor = Color.Transparent;
			nameLabel.ForeColor = Colors.CellDarkTextColor;
			nameLabel.TextAlign = ContentAlignment.MiddleCenter;
			nameLabel.Dock = DockStyle.Fill;
			nameLabel.UseCompatibleTextRendering = true;
			nameLabel.Text = "2048";
			m_UIPanel.Controls.Add(nameLabel, 0, 0);

			//placeholder
			var placeholder = new Panel();
			placeholder.Dock = DockStyle.Fill;
			m_UIPanel.Controls.Add(placeholder, 1, 0);

			//score panel
			m_UIScorePanel = new TableLayoutPanel();
			m_UIScorePanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100f));
			m_UIScorePanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100f));
			m_UIScorePanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 60f));
			m_UIScorePanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 40f));
			m_UIScorePanel.Dock = DockStyle.Fill;
			m_UIPanel.Controls.Add(m_UIScorePanel, 2, 0);

			var scoreLabel = new Label();
			scoreLabel.Font = new Font(m_customFonts.Families[0], 16, FontStyle.Bold);
			scoreLabel.AutoSize = false;
			scoreLabel.BackColor = Color.Transparent;
			scoreLabel.ForeColor = Colors.CellDarkTextColor;
			scoreLabel.TextAlign = ContentAlignment.MiddleRight;
			scoreLabel.Dock = DockStyle.Fill;
			scoreLabel.UseCompatibleTextRendering = true;
			scoreLabel.Text = "Score";
			m_UIScorePanel.Controls.Add(scoreLabel, 0, 0);

			var bestScoreLabel = new Label();
			bestScoreLabel.Font = new Font(m_customFonts.Families[0], 15, FontStyle.Bold);
			bestScoreLabel.AutoSize = false;
			bestScoreLabel.BackColor = Color.Transparent;
			bestScoreLabel.ForeColor = Colors.CellDarkTextColor;
			bestScoreLabel.TextAlign = ContentAlignment.MiddleRight;
			bestScoreLabel.Dock = DockStyle.Fill;
			bestScoreLabel.UseCompatibleTextRendering = true;
			bestScoreLabel.Text = "Best score";
			m_UIScorePanel.Controls.Add(bestScoreLabel, 0, 1);

			m_scoreValueLabel = new Label();
			m_scoreValueLabel.Font = new Font(m_customFonts.Families[0], 16, FontStyle.Bold);
			m_scoreValueLabel.AutoSize = false;
			m_scoreValueLabel.BackColor = Color.Transparent;
			m_scoreValueLabel.ForeColor = Colors.CellDarkTextColor;
			m_scoreValueLabel.TextAlign = ContentAlignment.MiddleLeft;
			m_scoreValueLabel.Padding = new Padding(30, 0, 0, 0);
			m_scoreValueLabel.Dock = DockStyle.Fill;
			m_scoreValueLabel.UseCompatibleTextRendering = true;
			m_scoreValueLabel.Text = "0";
			m_UIScorePanel.Controls.Add(m_scoreValueLabel, 1, 0);

			m_bestScoreValueLabel = new Label();
			m_bestScoreValueLabel.Font = new Font(m_customFonts.Families[0], 15, FontStyle.Bold);
			m_bestScoreValueLabel.AutoSize = false;
			m_bestScoreValueLabel.BackColor = Color.Transparent;
			m_bestScoreValueLabel.ForeColor = Colors.CellDarkTextColor;
			m_bestScoreValueLabel.TextAlign = ContentAlignment.MiddleLeft;
			m_bestScoreValueLabel.Padding = new Padding(30, 0, 0, 0);
			m_bestScoreValueLabel.Dock = DockStyle.Fill;
			m_bestScoreValueLabel.UseCompatibleTextRendering = true;
			m_bestScoreValueLabel.Text = "0";
			m_UIScorePanel.Controls.Add(m_bestScoreValueLabel, 1, 1);
		}

		private void InitField()
		{
			var config = m_gameModel.Config;

			m_FieldPanel = new TableLayoutPanel();
			for (int i = 0; i < config.SideSize; ++i)
			{
				m_FieldPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, ClientSize.Width / config.SideSize));
				m_FieldPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, ClientSize.Height * 0.8f / config.SideSize));
			}

			for (int row = 0; row < config.SideSize; ++row)
			{
				for (int column = 0; column < config.SideSize; ++column)
				{
					var panel = new RoundedPanel();
					panel.CurrentColor = Colors.EmptyCell;
					panel.Dock = DockStyle.Fill;
					panel.Margin = new Padding(10);
					panel.BorderRadius = 5;

					var valueLabel = new Label();
					valueLabel.Font = new Font(m_customFonts.Families[0], 45, FontStyle.Bold);
					valueLabel.AutoSize = false;
					valueLabel.BackColor = Color.Transparent;
					valueLabel.TextAlign = ContentAlignment.MiddleCenter;
					valueLabel.Dock = DockStyle.Fill;
					valueLabel.UseCompatibleTextRendering = true;
					panel.Controls.Add(valueLabel);

					m_FieldPanel.Controls.Add(panel, column, row);
				}
			}

			m_FieldPanel.BackColor = Colors.FieldColor;
			m_FieldPanel.Dock = DockStyle.Fill;
			m_LayoutPanel.Controls.Add(m_FieldPanel, 0, 1);
		}

		private void OnFieldChange(List<ChangeEvent> changeEvents)
		{
			foreach (var changeEvent in changeEvents)
			{
				var oldPos = changeEvent.PreviousPos;
				var newPos = changeEvent.NewPos;
				var newValue = changeEvent.Value;

				var prevPanel = m_FieldPanel.GetControlFromPosition(oldPos.Y, oldPos.X) as RoundedPanel;
				var newPanel = m_FieldPanel.GetControlFromPosition(newPos.Y, newPos.X) as RoundedPanel;

				//clear prev cell
				prevPanel.Controls[0].Text = "";
				prevPanel.CurrentColor = Colors.EmptyCell;

				//set new cell
				var label = newPanel.Controls[0] as Label;
				label.Text = (newValue > 0) ? newValue.ToString() : "";
				label.ForeColor = (newValue > 4) ? Colors.CellLightTextColor : Colors.CellDarkTextColor;
				if (newValue >= 1024 && label.Font.Size != 35)
					label.Font = new Font(m_customFonts.Families[0], 35, FontStyle.Bold);
				else if (newValue < 1024 && label.Font.Size != 45)
					label.Font = new Font(m_customFonts.Families[0], 45, FontStyle.Bold);

				newPanel.CurrentColor = Colors.CellColors[newValue];
			}

			//update score
			m_scoreValueLabel.Text = m_gameModel.Score.ToString();
			m_bestScoreValueLabel.Text = m_gameModel.BestScore.ToString();
			Settings.Default.BestScore = m_gameModel.BestScore;
			Settings.Default.Save();
		}

		private void OnStateChange()
		{
			bool result = false;
			if (m_gameModel.State == GameState.Losed)
				result = MessageBox.Show("You lose!\nStart new game?", 
					"Game over", MessageBoxButtons.YesNo) == DialogResult.Yes;
			else if (m_gameModel.State == GameState.Won)
				result = MessageBox.Show("You won!\nStart new game?",
					"Game over", MessageBoxButtons.YesNo) == DialogResult.Yes;

			if (result)
				m_gameModel.Start();
		}

		protected override void OnKeyDown(KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Up:
					m_gameModel.MakeMove(MoveDirection.Up);
					break;
				case Keys.Right:
					m_gameModel.MakeMove(MoveDirection.Right);
					break;
				case Keys.Down:
					m_gameModel.MakeMove(MoveDirection.Down);
					break;
				case Keys.Left:
					m_gameModel.MakeMove(MoveDirection.Left);
					break;
			}
		}
	}
}