﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Game2048
{
	static class Colors
	{
		public static Color BackgroundColor = Color.FromArgb(250, 248, 239);
		public static Color FieldColor = Color.FromArgb(187, 173, 160);

		public static Color EmptyCell = Color.FromArgb(205, 193, 180);
		public static Color FilledCell = Color.FromArgb(238, 228, 218);
		public static Dictionary<int, Color> CellColors = new Dictionary<int, Color>()
		{
			{ 0, EmptyCell },
			{ 2, Color.FromArgb(238, 228, 218) },
			{ 4, Color.FromArgb(237, 224, 200) },
			{ 8, Color.FromArgb(242, 177, 121) },
			{ 16, Color.FromArgb(245, 149, 99) },
			{ 32, Color.FromArgb(246, 124, 95) },
			{ 64, Color.FromArgb(246, 94, 59) },
			{ 128, Color.FromArgb(237, 207, 114) },
			{ 256, Color.FromArgb(237, 204, 97) },
			{ 512, Color.FromArgb(237, 200, 80) },
			{ 1024, Color.FromArgb(237, 197, 63) },
			{ 2048, Color.FromArgb(237, 194, 46) },
			{ 4096, Color.FromArgb(60, 58, 50) },
		};
		public static Color CellDarkTextColor = Color.FromArgb(119, 110, 101);
		public static Color CellLightTextColor = Color.FromArgb(249, 246, 242);
	}
}
