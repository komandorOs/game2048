﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Game2048.Model
{
	public struct GameConfiguration
	{
		public int SideSize { get; private set; }

		public GameConfiguration(int sideSize)
		{
			SideSize = sideSize;
		}
	}

	public enum CellType { Empty, Filled };
	public enum MoveDirection { Up, Right, Down, Left };
	public enum GameState { Idle, Playing, Won, Losed };

	public class GameCell
	{
		public CellType Type;
		public int Value;

		public GameCell(CellType type, int value)
		{
			Type = type;
			Value = value;
		}
	}

	public struct ChangeEvent
	{
		public Point PreviousPos;
		public Point NewPos;
		public int Value;

		public ChangeEvent(Point previousPos, Point newPos, int value)
		{
			PreviousPos = previousPos;
			NewPos = newPos;
			Value = value;
		}
	}

	public class GameModel
	{
		public GameConfiguration Config { get; private set; }

		public event Action<List<ChangeEvent>> FieldChanged;
		public event Action StateChanged;

		public int Score { get; private set; }
		public int BestScore { get; private set; }
		public GameState State { get; private set; }
		private GameCell[][] m_gameField; //jagged array a way performant than a standart multidimensional

		public GameModel(GameConfiguration gameConfig, int bestScore)
		{
			Config = gameConfig;

			//init field
			m_gameField = new GameCell[Config.SideSize][];
			for (int row = 0; row < Config.SideSize; ++row)
				m_gameField[row] = new GameCell[Config.SideSize];

			Score = 0;
			BestScore = bestScore;
			State = GameState.Idle;
		}

		/// <summary>
		/// Clears state and start new game
		/// </summary>
		public void Start()
		{
			Score = 0;
			State = GameState.Playing;

			ClearField();
			CreateStartingBlocks();
		}

		/// <summary>
		/// Start new game with specified field state (for testing purposes)
		/// </summary>
		public void Start(GameCell[][] initialField)
		{
			Score = 0;
			State = GameState.Playing;

			for (int row = 0; row < Config.SideSize; ++row)
				for (int column = 0; column < Config.SideSize; ++column)
					m_gameField[row][column] = initialField[row][column];
		}

		/// <summary>
		/// Fills field with CellType.Empty and 0 value
		/// </summary>
		private void ClearField()
		{
			var changeEvents = new List<ChangeEvent>();

			for (int row = 0; row < Config.SideSize; ++row)
			{
				for (int column = 0; column < Config.SideSize; ++column)
				{
					m_gameField[row][column] = new GameCell(CellType.Empty, 0);
					changeEvents.Add(new ChangeEvent(new Point(row, column), new Point(row, column), 0));
				}
			}

			if (FieldChanged != null)
				FieldChanged(changeEvents);
		}

		/// <summary>
		/// Create two random blocks on the field
		/// </summary>
		private void CreateStartingBlocks()
		{
			var gen = new Random();
			var firstPos = new Point(gen.Next(Config.SideSize), gen.Next(Config.SideSize));
			var secondPos = new Point(gen.Next(Config.SideSize), gen.Next(Config.SideSize));
			while (firstPos == secondPos)
				secondPos = new Point(gen.Next(Config.SideSize), gen.Next(Config.SideSize));

			m_gameField[firstPos.X][firstPos.Y] = new GameCell(CellType.Filled, 2);
			m_gameField[secondPos.X][secondPos.Y] = new GameCell(CellType.Filled, 2);


			if (FieldChanged != null)
			{
				var changeEvents = new List<ChangeEvent>();
				changeEvents.Add(new ChangeEvent(firstPos, firstPos, 2));
				changeEvents.Add(new ChangeEvent(secondPos, secondPos, 2));
				FieldChanged(changeEvents);
			}
		}

		/// <summary>
		/// Moves all cells on field in specified direction
		/// </summary>
		public void MakeMove(MoveDirection direction)
		{
			//do nothing when game is over
			if (State != GameState.Playing)
				return;

			bool isChanged = false;
			MoveCells(direction, ref isChanged);
			//do not create new block when no changes
			if (!isChanged)
				return;

			//if won after move
			if (State == GameState.Won)
			{
				if (StateChanged != null)
				{
					StateChanged();
					return;
				}
			}

			CreateNextRandomBlock();
			CheckGameover();
		}

		#region Moves methods (Up, Right, Down, Left)
		/// <summary>
		/// Move cells on the field in specified direction.
		/// Will set isChanged to true if there is at least one move
		/// </summary>
		private void MoveCells(MoveDirection direction, ref bool isChanged)
		{
			switch (direction)
			{
				case MoveDirection.Up:
					MoveCellsUp(ref isChanged);
					break;
				case MoveDirection.Right:
					MoveCellsRight(ref isChanged);
					break;
				case MoveDirection.Down:
					MoveCellsDown(ref isChanged);
					break;
				case MoveDirection.Left:
					MoveCellsLeft(ref isChanged);
					break;
			}
		}

		private bool TryMergeBlock(GameCell lastBlock, ref int value)
		{
			if (lastBlock.Type == CellType.Filled)
			{
				if (lastBlock.Value == value)
				{
					value += lastBlock.Value;
					Score += value;
					if (Score > BestScore)
						BestScore = Score;
				}
				else
					return false;
			}

			if (value == 2048)
				State = GameState.Won;

			return true;
		}

		//There is DRY violation, but
		//4 simple methods are better than complex algorithm with many parameters
		private void MoveCellsUp(ref bool changed)
		{
			var changeEvents = new List<ChangeEvent>();
			
			//start from second row
			for (int row = 1; row < Config.SideSize; ++row)
				for (int column = 0; column < Config.SideSize; ++column)
				{
					var currentCell = m_gameField[row][column];
					int value = m_gameField[row][column].Value;

					if (currentCell.Type == CellType.Empty)
						continue;

					int rowDelta = 1;
					while (row - rowDelta > 0 &&
						   m_gameField[row - rowDelta][column].Type == CellType.Empty)
						rowDelta += 1;

					var lastBlock = m_gameField[row - rowDelta][column];
					bool result = TryMergeBlock(lastBlock, ref value);
					if (!result)
						rowDelta -= 1;

					//no movement
					if (rowDelta == 0)
						continue;

					m_gameField[row][column] = new GameCell(CellType.Empty, 0);
					m_gameField[row - rowDelta][column] = new GameCell(CellType.Filled, value);

					changed = true;

					changeEvents.Add(new ChangeEvent(new Point(row, column), new Point(row - rowDelta, column), value));
				}

			if (FieldChanged != null && changeEvents.Count != 0)
				FieldChanged(changeEvents);
		}

		private void MoveCellsRight(ref bool changed)
		{
			var changeEvents = new List<ChangeEvent>();

			//start from second column
			for (int column = Config.SideSize - 2; column >= 0; --column)
				for (int row = Config.SideSize - 1; row >= 0; --row)
				{
					var currentCell = m_gameField[row][column];
					int value = m_gameField[row][column].Value;

					if (currentCell.Type == CellType.Empty)
						continue;

					int columnDelta = 1;
					while (column + columnDelta < Config.SideSize - 1 &&
						   m_gameField[row][column + columnDelta].Type == CellType.Empty)
						columnDelta += 1;

					var lastBlock = m_gameField[row][column + columnDelta];
					bool result = TryMergeBlock(lastBlock, ref value);
					if (!result)
						columnDelta -= 1;

					//no movement
					if (columnDelta == 0)
						continue;

					m_gameField[row][column] = new GameCell(CellType.Empty, 0);
					m_gameField[row][column + columnDelta] = new GameCell(CellType.Filled, value);

					changed = true;

					changeEvents.Add(new ChangeEvent(new Point(row, column), new Point(row, column + columnDelta), value));
				}

			if (FieldChanged != null && changeEvents.Count != 0)
				FieldChanged(changeEvents);
		}

		private void MoveCellsDown(ref bool changed)
		{
			var changeEvents = new List<ChangeEvent>();

			//start from second row
			for (int row = Config.SideSize - 2; row >= 0; --row)
				for (int column = Config.SideSize - 1; column >= 0; --column)
				{
					var currentCell = m_gameField[row][column];
					int value = m_gameField[row][column].Value;

					if (currentCell.Type == CellType.Empty)
						continue;

					int rowDelta = 1;
					while (row + rowDelta < Config.SideSize - 1 &&
						   m_gameField[row + rowDelta][column].Type == CellType.Empty)
						rowDelta += 1;

					var lastBlock = m_gameField[row + rowDelta][column];
					bool result = TryMergeBlock(lastBlock, ref value);
					if (!result)
						rowDelta -= 1;

					//no movement
					if (rowDelta == 0)
						continue;

					m_gameField[row][column] = new GameCell(CellType.Empty, 0);
					m_gameField[row + rowDelta][column] = new GameCell(CellType.Filled, value);

					changed = true;

					changeEvents.Add(new ChangeEvent(new Point(row, column), new Point(row + rowDelta, column), value));
				}

			if (FieldChanged != null && changeEvents.Count != 0)
				FieldChanged(changeEvents);
		}

		private void MoveCellsLeft(ref bool changed)
		{
			var changeEvents = new List<ChangeEvent>();

			//start from second column
			for (int column = 1; column < Config.SideSize; ++column)
				for (int row = 0; row < Config.SideSize; ++row)
				{
					var currentCell = m_gameField[row][column];
					int value = m_gameField[row][column].Value;

					if (currentCell.Type == CellType.Empty)
						continue;

					int columnDelta = 1;
					while (column - columnDelta > 0 &&
						   m_gameField[row][column - columnDelta].Type == CellType.Empty)
						columnDelta += 1;

					var lastBlock = m_gameField[row][column - columnDelta];
					bool result = TryMergeBlock(lastBlock, ref value);
					if (!result)
						columnDelta -= 1;

					//no movement
					if (columnDelta == 0)
						continue;

					m_gameField[row][column] = new GameCell(CellType.Empty, 0);
					m_gameField[row][column - columnDelta] = new GameCell(CellType.Filled, value);

					changed = true;

					changeEvents.Add(new ChangeEvent(new Point(row, column), new Point(row, column - columnDelta), value));
				}

			if (FieldChanged != null && changeEvents.Count != 0)
				FieldChanged(changeEvents);
		}
#endregion

		/// <summary>
		/// Returns free random position for new block.
		/// If there is no empty cells, returns null
		/// </summary>
		private Point? TryGetNewBlockPosition()
		{
			List<Point> emptyCells = new List<Point>();
			for (int row = 0; row < Config.SideSize; ++row)
				for (int column = 0; column < Config.SideSize; ++column)
					if (m_gameField[row][column].Type == CellType.Empty)
						emptyCells.Add(new Point(row, column));

			if (emptyCells.Count == 0)
				return null;

			var gen = new Random();
			return emptyCells[gen.Next(emptyCells.Count)];
		}

		/// <summary>
		/// Create new block in free random position on the field
		/// </summary>
		private void CreateNextRandomBlock()
		{
			var newBlockPos = (Point)TryGetNewBlockPosition();
			m_gameField[newBlockPos.X][newBlockPos.Y] = new GameCell(CellType.Filled, 2);

			if (FieldChanged != null)
				FieldChanged(new List<ChangeEvent>() { new ChangeEvent(newBlockPos, newBlockPos, 2) });
		}

		/// <summary>
		/// Checks field situation for a gameover
		/// </summary>
		private void CheckGameover()
		{
			var result = TryGetNewBlockPosition();
			if (result == null)
			{
				if (!IsAnyAvailableMove())
				{
					State = GameState.Losed;
					if (StateChanged != null)
						StateChanged();
				}
			}
		}

		/// <summary>
		/// Checks if there is at least one available move on the field
		/// </summary>
		private bool IsAnyAvailableMove()
		{
			var leftTop = new[] { MoveDirection.Right, MoveDirection.Down };
			var rightTop = new[] { MoveDirection.Left, MoveDirection.Down };
			var leftBottom = new[] { MoveDirection.Right, MoveDirection.Up };
			var rightBottom = new[] { MoveDirection.Left, MoveDirection.Up };
			var top = new[] { MoveDirection.Right, MoveDirection.Down, MoveDirection.Left };
			var left = new[] { MoveDirection.Up, MoveDirection.Right, MoveDirection.Down };
			var right = new[] { MoveDirection.Up, MoveDirection.Left, MoveDirection.Down };
			var down = new[] { MoveDirection.Up, MoveDirection.Right, MoveDirection.Left };
			var all = new[] { MoveDirection.Right, MoveDirection.Down, MoveDirection.Left, MoveDirection.Up };

			for (int row = 0; row < Config.SideSize; ++row)
			{
				for (int column = 0; column < Config.SideSize; ++column)
				{
					MoveDirection[] directions;

					if (row == 0 && column == 0)
						directions = leftTop;
					else if (row == 0 && column == Config.SideSize - 1)
						directions = rightTop;
					else if (row == Config.SideSize - 1 && column == 0)
						directions = leftBottom;
					else if (row == Config.SideSize - 1 && column == Config.SideSize - 1)
						directions = rightBottom;
					else if (row == 0)
						directions = top;
					else if (column == 0)
						directions = left;
					else if (row == Config.SideSize - 1)
						directions = down;
					else if (column == Config.SideSize - 1)
						directions = right;
					else
						directions = all;

					for (int i = 0; i < directions.Length; ++i)
					{
						switch (directions[i])
						{
							case MoveDirection.Up:
								if (m_gameField[row - 1][column].Value == m_gameField[row][column].Value)
									return true;
								break;
							case MoveDirection.Right:
								if (m_gameField[row][column + 1].Value == m_gameField[row][column].Value)
									return true;
								break;
							case MoveDirection.Down:
								if (m_gameField[row + 1][column].Value == m_gameField[row][column].Value)
									return true;
								break;
							case MoveDirection.Left:
								if (m_gameField[row][column - 1].Value == m_gameField[row][column].Value)
									return true;
								break;
						}
					}
				}
			}

			return false;
		}
	}
}