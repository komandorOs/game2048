﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Game2048.Model;
using System.Drawing;
using System.Collections.Generic;

namespace Game2048.Tests
{
	[TestClass]
	public class GameModelTests
	{
		#region Helpers
		private GameCell[][] MakeGameFieldFromIntArray(int[][] initial)
		{
			var gameField = new GameCell[initial.Length][];

			for (int row = 0; row < initial.Length; ++row)
			{
				gameField[row] = new GameCell[initial[row].Length];
				for (int column = 0; column < initial[row].Length; ++column)
					gameField[row][column] =
						new GameCell(initial[row][column] == 0 ? CellType.Empty : CellType.Filled,
									 initial[row][column]);
			}

			return gameField;
		}

		private GameModel CreateGameModel(GameCell[][] initialGameField)
		{
			var config = new GameConfiguration(4);

			var model = new GameModel(config, 0);
			model.Start(initialGameField);
			return model;
		}

		private void OnFieldChanged(List<ChangeEvent> changeEvents, int[][] currentField)
		{
			foreach (var changeEvent in changeEvents)
			{
				if (changeEvent.PreviousPos != changeEvent.NewPos) //prevPos == newPos when inserting new random block
				{
					currentField[changeEvent.PreviousPos.X][changeEvent.PreviousPos.Y] = 0;
					currentField[changeEvent.NewPos.X][changeEvent.NewPos.Y] = changeEvent.Value;
				}
			}
		}

		private bool IsEqual(int[][] expected, int[][] actual)
		{
			if (expected.Length != actual.Length)
				throw new ArgumentException();

			for (int row = 0; row < expected.Length; ++row)
				for (int column = 0; column < expected[row].Length; ++column)
					if (expected[row][column] != actual[row][column])
					return false;

			return true;
		}
		#endregion

		[TestMethod]
		public void MoveUp()
		{
			var initial = new int[][]
			{
				new int[] { 2, 4, 0,  2 },
				new int[] { 2, 0, 0,  0 },
				new int[] { 0, 4, 0,  0 },
				new int[] { 0, 0, 16, 4 }
			};
			var expected = new int[][]
			{
				new int[] { 4, 8, 16, 2 },
				new int[] { 0, 0, 0,  4 },
				new int[] { 0, 0, 0,  0 },
				new int[] { 0, 0, 0,  0 }
			};

			var model = CreateGameModel(MakeGameFieldFromIntArray(initial));
			model.FieldChanged += (changeEvents) => OnFieldChanged(changeEvents, initial);
			model.MakeMove(MoveDirection.Up);

			Assert.IsTrue(IsEqual(expected, initial));
		}

		[TestMethod]
		public void MoveRight()
		{
			var initial = new int[][]
			{
				new int[] { 0,  0, 2, 2 },
				new int[] { 0,  4, 0, 4 },
				new int[] { 16, 0, 0, 0 },
				new int[] { 4,  0, 0, 2 }
			};
			var expected = new int[][]
			{
				new int[] { 0, 0, 0, 4  },
				new int[] { 0, 0, 0, 8  },
				new int[] { 0, 0, 0, 16 },
				new int[] { 0, 0, 4, 2  }
			};

			var model = CreateGameModel(MakeGameFieldFromIntArray(initial));
			model.FieldChanged += (changeEvents) => OnFieldChanged(changeEvents, initial);
			model.MakeMove(MoveDirection.Right);

			Assert.IsTrue(IsEqual(expected, initial));
		}

		[TestMethod]
		public void MoveDown()
		{
			var initial = new int[][]
			{
				new int[] { 0, 0, 16, 0 },
				new int[] { 0, 4, 0,  4 },
				new int[] { 2, 0, 0,  0 },
				new int[] { 2, 4, 0,  2 }
			};
			var expected = new int[][]
			{
				new int[] { 0, 0, 0,  0 },
				new int[] { 0, 0, 0,  0 },
				new int[] { 0, 0, 0,  4 },
				new int[] { 4, 8, 16, 2 }
			};

			var model = CreateGameModel(MakeGameFieldFromIntArray(initial));
			model.FieldChanged += (changeEvents) => OnFieldChanged(changeEvents, initial);
			model.MakeMove(MoveDirection.Down);

			Assert.IsTrue(IsEqual(expected, initial));
		}

		[TestMethod]
		public void MoveLeft()
		{
			var initial = new int[][]
			{
				new int[] { 2, 2, 0, 0  },
				new int[] { 4, 0, 4, 0  },
				new int[] { 0, 0, 0, 16 },
				new int[] { 2, 0, 0, 4  }
			};
			var expected = new int[][]
			{
				new int[] { 4,  0, 0, 0 },
				new int[] { 8,  0, 0, 0 },
				new int[] { 16, 0, 0, 0 },
				new int[] { 2,  4, 0, 0 }
			};

			var model = CreateGameModel(MakeGameFieldFromIntArray(initial));
			model.FieldChanged += (changeEvents) => OnFieldChanged(changeEvents, initial);
			model.MakeMove(MoveDirection.Left);

			Assert.IsTrue(IsEqual(expected, initial));
		}

		[TestMethod]
		public void IncreaseScore()
		{
			var initial = new int[][]
			{
				new int[] { 2, 2, 0, 0 },
				new int[] { 4, 4, 0, 0 },
				new int[] { 0, 0, 0, 0 },
				new int[] { 2, 4, 0, 0 }
			};

			var model = CreateGameModel(MakeGameFieldFromIntArray(initial));
			model.MakeMove(MoveDirection.Left);

			Assert.AreEqual(model.Score, 12);
		}

		[TestMethod]
		public void Win()
		{
			var initial = new int[][]
			{
				new int[] { 1024, 1024, 0, 0 },
				new int[] { 0,    0,    0, 0 },
				new int[] { 0,    0,    0, 0 },
				new int[] { 0,    0,    0, 0 }
			};
			var expected = new int[][]
			{
				new int[] { 2048, 0, 0, 0 },
				new int[] { 0,    0, 0, 0 },
				new int[] { 0,    0, 0, 0 },
				new int[] { 0,    0, 0, 0 }
			};

			var model = CreateGameModel(MakeGameFieldFromIntArray(initial));
			model.FieldChanged += (changeEvents) => OnFieldChanged(changeEvents, initial);
			model.MakeMove(MoveDirection.Left);

			Assert.IsTrue(IsEqual(expected, initial));
			Assert.AreEqual(model.State, GameState.Won);
		}

		[TestMethod]
		public void Loose()
		{
			var initial = new int[][]
			{
				new int[] { 2, 4,  8,  2 },
				new int[] { 4, 16, 32, 16 },
				new int[] { 8, 64, 4,  2 },
				new int[] { 2, 2,  8,  32 }
			};
			var expected = new int[][]
			{
				new int[] { 2, 4,  8,  2 },
				new int[] { 4, 16, 32, 16 },
				new int[] { 8, 64, 4,  2 },
				new int[] { 0, 4,  8,  32 }
			};

			var model = CreateGameModel(MakeGameFieldFromIntArray(initial));
			model.FieldChanged += (changeEvents) => OnFieldChanged(changeEvents, initial);
			model.MakeMove(MoveDirection.Right);

			Assert.IsTrue(IsEqual(expected, initial));
			Assert.AreEqual(model.State, GameState.Losed);
		}
	}
}
